package com.example.peopleflowdemo.employeemanagement;

import com.example.peopleflowdemo.commonlib.kafka.dto.EmployeeKafkaDto;
import com.example.peopleflowdemo.commonlib.kafka.dto.ResultKafkaDto;
import com.example.peopleflowdemo.commonlib.mapping.IEmployeeMapper;
import com.example.peopleflowdemo.commonlib.repository.EmployeeRepository;
import com.example.peopleflowdemo.commonlib.repository.entity.EmployeeEntity;
import com.example.peopleflowdemo.commonlib.service.helper.EmployeeMappingHelper;
import com.example.peopleflowdemo.commonlib.statemachine.event.StateEvent;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeePost;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeeStatePatch;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.ZonedDateTime;
import java.util.Locale;

import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Import(Utils.class)
@SpringBootTest(
        args = {"--spring.config.name=bootstrap,employee-management,employee-management-access,common-config"}
)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Testcontainers
@EmbeddedKafka(partitions = 1, brokerProperties = {"listeners=PLAINTEXT://localhost:9092", "port=9092"})
@ContextConfiguration(initializers = MyPostgreSQLContainerInitializer.class)
class UnlimitedEmployeeApiTest {


    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private Utils utils;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private EmployeeRepository repository;
    @Autowired
    private IEmployeeMapper mapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static Long generatedEmployeeId;


    @AfterEach
    void tearDown() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "employees");
    }


    @Test
    void postgreSQLContainerRun_test() {
        assertTrue(MyPostgreSQLContainerInitializer.postgreSQLContainer.isRunning());
    }


    @Test
    void forbidden_test() throws Exception {
        String forbiddenJwt = utils.jwtFromJsonFile("jwt/denyPayload.json");

        mockMvc.perform(post("/unlimited/employee")
                .content(utils.asJsonString(new EmployeePost()))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + forbiddenJwt))
                .andExpect(status().isForbidden());
    }


    @Test
    void minAge_validation_test() throws Exception {
        String permittedJwt = utils.jwtFromJsonFile("jwt/permitPayload.json");

        String expectedValidationMessage = messageSource.getMessage("employeePost.age.min", null, Locale.getDefault());
        mockMvc.perform(post("/unlimited/employee")
                .content(utils.bytesFromTestResources("body/EmployeePost_invalidMinAge.json"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + permittedJwt))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.validationMessage.age", startsWith(expectedValidationMessage)));
    }


    @Test
    void patchEmployeeAfterAdding_test() throws Exception {
        String permittedJwt = utils.jwtFromJsonFile("jwt/permitPayload.json");

        generatedEmployeeId = 0L;

        mockMvc.perform(patch("/unlimited/employee/state/{employeeId}", generatedEmployeeId)
                .content(utils.asJsonString(new EmployeeStatePatch(StateEvent.CHECK.name())))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + permittedJwt))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.code").value("EMPMAN_1"));

        mockMvc.perform(post("/unlimited/employee")
                .content(utils.bytesFromTestResources("body/EmployeePost_valid.json"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + permittedJwt))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.employeeId").value(generatedEmployeeId));

        mockMvc.perform(post("/unlimited/employee")
                .content(utils.bytesFromTestResources("body/EmployeePost_valid.json"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + permittedJwt))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code").value("EMPMAN_4"));

        mockMvc.perform(patch("/unlimited/employee/state/{employeeId}", generatedEmployeeId)
                .content(utils.asJsonString(new EmployeeStatePatch(StateEvent.CHECK.name())))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + permittedJwt))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.code").value("EMPMAN_3"));

        mockMvc.perform(get("/unlimited/employee")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + permittedJwt))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.[0].employeeId").value(generatedEmployeeId));
    }


    @SendTo
    @KafkaListener(topics = "${kafka-topics.employee-add.name}", groupId = "${groups.default-employee-add}")
    public ResultKafkaDto listen(@Payload EmployeeKafkaDto data) {
        EmployeeEntity entity = repository.save(mapper.toEntity(data, EmployeeMappingHelper.createEntityHelper(ZonedDateTime.now())));
        generatedEmployeeId = entity.getEmployeeId();
        return ResultKafkaDto.createSuccess(entity.getEmployeeId(), entity.getOpenDate());
    }


}