package com.example.peopleflowdemo.employeemanagement;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

public class MyPostgreSQLContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {


    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer(
            DockerImageName.parse("mishgun8ku/peopleflow-db").asCompatibleSubstituteFor("postgres"))
            .withPassword("postgres")
            .withUsername("postgres")
            .withDatabaseName("postgres");


    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        postgreSQLContainer.start();
        System.setProperty("DB_URL", postgreSQLContainer.getJdbcUrl());
    }
}