package com.example.peopleflowdemo.employeemanagement;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.interfaces.RSAPrivateKey;
import java.util.Map;


@Component
public class Utils {


    public String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public byte[] bytesFromTestResources(String fileName) throws IOException {
        String path = "src/test/resources/" + fileName;
        File file = new File(path);
        return FileCopyUtils.copyToByteArray(file);
    }


    public String jwtFromMap(Map<String, ?> tokenData) {

        JWSSigner jwsSigner = new RSASSASigner(privateKey);


        JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder();
        tokenData.forEach(builder::claim);
        JWTClaimsSet claimsSet = builder.build();

        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.RS256), claimsSet);

        try {
            signedJWT.sign(jwsSigner);
        } catch (JOSEException e) {
            e.printStackTrace();
        }

        String s = signedJWT.serialize();
        return s;
    }


    public String jwtFromJsonFile(String fileName) throws IOException {

        JWSSigner jwsSigner = new RSASSASigner(privateKey);

        String payloadStr = new String(Files.readAllBytes(Paths.get("src/test/resources/" + fileName)));

        JWSObject jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.RS256),
                new Payload(payloadStr));

        try {
            jwsObject.sign(jwsSigner);
        } catch (JOSEException e) {
            e.printStackTrace();
        }

        String s = jwsObject.serialize();
        return s;
    }


    @Value("${spring.security.oauth2.resourceserver.jwt.private-key-location}")
    private RSAPrivateKey privateKey;


}
