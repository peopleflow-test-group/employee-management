package com.example.peopleflowdemo.employeemanagement;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.crypto.RsaKeyConversionServicePostProcessor;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


@Import(Utils.class)
@SpringBootTest(
        classes = {
                Utils.class,
                RsaKeyConversionServicePostProcessor.class
        },
        args = {"--spring.config.name=bootstrap,employee-management,employee-management-access,common-config",
                "--spring.profiles.active=test"}
)
class jwtGenTest {


    @Test
    void just_give_me_jwt_from_map_test() {
        Map<String, ?> map = new HashMap<>() {{
            put("user_name", "custom_unknown_person");
            put("roles", Arrays.asList("manager_role", "employee_role"));
        }};

        String jwtStr = utils.jwtFromMap(map);
        System.out.println("jwt from map: " + jwtStr);
        Assertions.assertFalse(jwtStr.isEmpty());
    }


    @Test
    void just_give_me_jwt_from_json_file_test() throws IOException {
        String jwtStr = utils.jwtFromJsonFile("jwt/permitPayload.json");
        System.out.println("jwt from file: " + jwtStr);
        Assertions.assertFalse(jwtStr.isEmpty());
    }


//    @Test
//    void just_test() throws IOException {
//        String phoneNumber = "555";
//        System.out.println(MessageFormat
//                .format("Phone number {0} already exists", phoneNumber));
//    }


    @Autowired
    private Utils utils;


}

