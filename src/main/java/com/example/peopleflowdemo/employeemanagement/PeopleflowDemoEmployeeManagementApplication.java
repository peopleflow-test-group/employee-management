package com.example.peopleflowdemo.employeemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.example.peopleflowdemo.commonlib.repository")
@EntityScan(basePackages = "com.example.peopleflowdemo.commonlib.repository")
@ComponentScan(basePackages = {
        "com.example.peopleflowdemo.commonlib",
        "com.example.peopleflowdemo.employeemanagement"})
@SpringBootApplication
public class PeopleflowDemoEmployeeManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(PeopleflowDemoEmployeeManagementApplication.class, args);
    }

}
