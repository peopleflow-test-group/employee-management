package com.example.peopleflowdemo.employeemanagement.config;

import com.example.peopleflowdemo.commonlib.kafka.dto.ChangeEmployeeStateKafkaDto;
import com.example.peopleflowdemo.commonlib.kafka.dto.EmployeeKafkaDto;
import com.example.peopleflowdemo.commonlib.kafka.dto.ResultKafkaDto;
import com.example.peopleflowdemo.commonlib.web.dto.common.CommonResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.config.TopicConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * kafka init config
 */
@Configuration
public class KafkaConfig {


    @Value("${groups.default-employee-add-result}")
    private String employeeAddResultGroup;


    @Bean
    public ReplyingKafkaTemplate<String, EmployeeKafkaDto, ResultKafkaDto> replyingKafkaTemplate(ProducerFactory<String, EmployeeKafkaDto> pf,
                                                                                                 ConcurrentKafkaListenerContainerFactory<String, ResultKafkaDto> factory) {
        ConcurrentMessageListenerContainer<String, ResultKafkaDto> replyContainer = factory.createContainer(kafkaTopicMap().get("employee-add-result").getName());
        replyContainer.getContainerProperties().setGroupId(employeeAddResultGroup);
        return new ReplyingKafkaTemplate<>(pf, replyContainer);
    }


    @Bean
    public KafkaTemplate<String, ChangeEmployeeStateKafkaDto> changeStateTemplate(ProducerFactory<String, ChangeEmployeeStateKafkaDto> pf) {
        return new KafkaTemplate<>(pf);
    }


    @Bean
    public KafkaTemplate<String, ResultKafkaDto> replyTemplate(ProducerFactory<String, ResultKafkaDto> pf,
                                                       ConcurrentKafkaListenerContainerFactory<String, ResultKafkaDto> factory) {
        KafkaTemplate<String, ResultKafkaDto> kafkaTemplate = new KafkaTemplate<>(pf);
        factory.getContainerProperties().setMissingTopicsFatal(false);
        factory.setReplyTemplate(kafkaTemplate);
        return kafkaTemplate;
    }


    @Bean
    public NewTopic employeeStateTopic() {
        TopicProps props = kafkaTopicMap().get("employee-state");
        return TopicBuilder.name(props.getName())
                .partitions(props.getPartitions())
                .replicas(props.getReplicas())
                .config(TopicConfig.MIN_IN_SYNC_REPLICAS_CONFIG, props.getIsr())
                .build();
    }


    @Bean
    public NewTopic employeeAddTopic() {
        TopicProps props = kafkaTopicMap().get("employee-add");
        return TopicBuilder.name(props.getName())
                .partitions(props.getPartitions())
                .replicas(props.getReplicas())
                .config(TopicConfig.MIN_IN_SYNC_REPLICAS_CONFIG, props.getIsr())
                .build();
    }


    @Bean
    public NewTopic employeeAddResultTopic() {
        TopicProps props = kafkaTopicMap().get("employee-add-result");
        return TopicBuilder.name(props.getName())
                .partitions(props.getPartitions())
                .replicas(props.getReplicas())
                .config(TopicConfig.MIN_IN_SYNC_REPLICAS_CONFIG, props.getIsr())
                .build();
    }


    /**
     * bean based on external properties
     *
     * @return stores topics properties
     */
    @Bean(name = "kafkaTopicMap")
    @ConfigurationProperties(prefix = "kafka-topics")
    public Map<String, TopicProps> kafkaTopicMap() {
        return new HashMap<>();
    }


    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    private static class TopicProps {
        private String name;
        private int partitions;
        private int replicas;
        private String isr;
    }

}
