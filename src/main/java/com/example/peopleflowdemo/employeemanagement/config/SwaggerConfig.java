package com.example.peopleflowdemo.employeemanagement.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.HeaderParameter;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * swagger doc editing
 */
@Configuration
public class SwaggerConfig {

    /**
     * adds centralized bearer token input
     *
     * @return updated OpenAPI scheme
     */
    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("PeopleFlow OpenAPI definition"))
                .components(new Components()
                        .addSecuritySchemes("bearer-key", new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("bearer").bearerFormat("JWT")));
    }


    /**
     * adds global header
     *
     * @return updated customizer
     */
    @Bean
    public OpenApiCustomiser consumerTypeHeaderOpenAPICustomiser() {
        return openApi -> openApi.getPaths().values().stream().flatMap(pathItem -> pathItem.readOperations().stream())
                .forEach(operation -> operation.addParametersItem(new HeaderParameter()
                        .name("Accept-Language")
                        .schema(new Schema<>().type("string"))
                ));
    }

}
