package com.example.peopleflowdemo.employeemanagement.config;

import com.example.peopleflowdemo.employeemanagement.web.filter.ContainerLoggingFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.example.peopleflowdemo.employeemanagement.util.ConstUtil.APP_MIN_FILTER_ORDER;

/**
 * request/response logging filter registration
 */
@Configuration
public class ContainerLogFilterConfig {

    @Bean
    public FilterRegistrationBean<ContainerLoggingFilter> containerLoggingFilterRegBean() {
        FilterRegistrationBean<ContainerLoggingFilter> bean = new FilterRegistrationBean<>();

        bean.setFilter(new ContainerLoggingFilter());
        bean.setOrder(APP_MIN_FILTER_ORDER + 2);

        return bean;
    }

}
