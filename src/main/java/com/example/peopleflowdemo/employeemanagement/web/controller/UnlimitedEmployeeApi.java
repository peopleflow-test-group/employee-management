package com.example.peopleflowdemo.employeemanagement.web.controller;

import com.example.peopleflowdemo.commonlib.web.dto.EmployeePost;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeeResponse;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeeStatePatch;
import com.example.peopleflowdemo.commonlib.web.dto.common.CommonResponse;
import com.example.peopleflowdemo.commonlib.web.dto.common.ConstraintResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.List;

@Validated
public interface UnlimitedEmployeeApi {

    @Operation(
            security = {@SecurityRequirement(name = "bearer-key")},
            summary = "Adding an employee by manager.", tags = {"Employee"}, description = "An endpoint for manager to support adding an employee with very basic employee details including (name, contract information, age, you can decide.) With initial state \"ADDED\" which indicates that the employee isn't active yet.",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Ok", content = @Content(mediaType = "application/json", schema = @Schema(implementation = EmployeeResponse.class))),
                    @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(mediaType = "application/json", schema = @Schema(implementation = CommonResponse.class))),
                    @ApiResponse(responseCode = "422", description = "Unprocessable Entity", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ConstraintResponse.class))),
                    @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content())
            })
    @PostMapping(value = "/unlimited/employee", consumes = "application/json", produces = "application/json")
    EmployeeResponse unlimitedEmployeePost(@RequestBody @Validated EmployeePost body);


    @Operation(
            security = {@SecurityRequirement(name = "bearer-key")},
            summary = "Change the state of a given employee.", tags = {"Employee"}, description = "An endpoint for manager to support changing the state of a given employee.",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Ok", content = @Content(mediaType = "application/json", schema = @Schema(implementation = CommonResponse.class))),
                    @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(mediaType = "application/json", schema = @Schema(implementation = CommonResponse.class))),
                    @ApiResponse(responseCode = "422", description = "Unprocessable Entity", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ConstraintResponse.class))),
                    @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content())
            })
    @PatchMapping(value = "/unlimited/employee/state/{employeeId}", consumes = "application/json", produces = "application/json")
    CommonResponse unlimitedEmployeeStatePatch(@PathVariable("employeeId") Long employeeId, @RequestBody @Validated EmployeeStatePatch body);


    @Operation(
            security = {@SecurityRequirement(name = "bearer-key")},
            summary = "Get employee by phone number.", tags = {"Employee"}, description = "Get employee details by known phone number for manager.",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Ok", content = @Content(mediaType = "application/json", schema = @Schema(implementation = EmployeeResponse.class))),
                    @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(mediaType = "application/json", schema = @Schema(implementation = CommonResponse.class))),
                    @ApiResponse(responseCode = "404", description = "Bad request", content = @Content(mediaType = "application/json", schema = @Schema(implementation = CommonResponse.class))),
                    @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content())
            })
    @GetMapping(value = "/unlimited/employee/{mobilePhone}", produces = "application/json")
    EmployeeResponse unlimitedEmployeePhoneNumberGet(@Valid @PathVariable("mobilePhone") @Pattern(regexp = "\\d{12}", message = "{mobilePhone.pattern} {regexp}") String mobilePhone);


    @Operation(
            security = {@SecurityRequirement(name = "bearer-key")},
            summary = "Get all employees.", tags = {"Employee"}, description = "Get all employees for manager.",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Ok", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = EmployeeResponse.class)))),
                    @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content())
            })
    @GetMapping(value = "/unlimited/employee", produces = "application/json")
    List<EmployeeResponse> unlimitedEmployeeGet();
}
