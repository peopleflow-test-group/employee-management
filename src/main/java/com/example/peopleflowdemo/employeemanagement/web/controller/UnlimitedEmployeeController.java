package com.example.peopleflowdemo.employeemanagement.web.controller;

import com.example.peopleflowdemo.commonlib.exception.BaseRestResponseException;
import com.example.peopleflowdemo.commonlib.mapping.IEmployeeMapper;
import com.example.peopleflowdemo.commonlib.repository.entity.EmployeeEntity;
import com.example.peopleflowdemo.commonlib.service.IReadEmployeeService;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeePost;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeeResponse;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeeStatePatch;
import com.example.peopleflowdemo.commonlib.web.dto.common.CommonResponse;
import com.example.peopleflowdemo.employeemanagement.service.IAsyncEmployeeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.context.i18n.LocaleContextHolder.getLocale;
import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * only a simple logic with a transactional service behind
 */
@RequiredArgsConstructor
@Slf4j
@RestController
public class UnlimitedEmployeeController implements UnlimitedEmployeeApi {

    private final IAsyncEmployeeService asyncEmployeeService;
    private final IReadEmployeeService readEmployeeService;
    private final MessageSource messageSource;
    private final IEmployeeMapper mapper;


    @Override
    public EmployeeResponse unlimitedEmployeePost(EmployeePost body) {
        return asyncEmployeeService.create(body);
    }


    @Override
    public CommonResponse unlimitedEmployeeStatePatch(Long employeeId, EmployeeStatePatch body) {

        asyncEmployeeService.update(employeeId, body);

        return new CommonResponse(
                "EMPMAN_3",
                null,
                messageSource.getMessage("EMPMAN_3", new Object[]{body.getEvent()}, getLocale()));
    }


    @Override
    public EmployeeResponse unlimitedEmployeePhoneNumberGet(String mobilePhone) {

        EmployeeEntity entity = readEmployeeService.getByMobilePhoneOrElseThrow(mobilePhone, () -> new BaseRestResponseException(
                NOT_FOUND.value(),
                "EMPMAN_5",
                NOT_FOUND.getReasonPhrase(),
                NOT_FOUND.getReasonPhrase(),
                messageSource.getMessage("EMPMAN_5", null, getLocale())));

        return mapper.toResponse(entity);
    }


    @Override
    public List<EmployeeResponse> unlimitedEmployeeGet() {

        List<EmployeeEntity> employeeEntityList = readEmployeeService.getAllEmployees();

        return mapper.toResponseList(employeeEntityList);
    }
}
