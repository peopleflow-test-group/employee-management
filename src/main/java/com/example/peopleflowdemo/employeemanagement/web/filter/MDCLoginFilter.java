package com.example.peopleflowdemo.employeemanagement.web.filter;

import com.nimbusds.jwt.JWTParser;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.http.HttpHeaders;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * retrieves user_name from jwt and puts into logger attributes
 */
@Slf4j
public class MDCLoginFilter extends OncePerRequestFilter {


    private static final String LOGIN_MDC_ATTR_NAME = "user";
    private static final String BEARER_PREFIX = "Bearer ";


    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        String login = null;
        try {
            String authHeader = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
            if (authHeader != null && authHeader.startsWith(BEARER_PREFIX)) {
                String jwtStr = authHeader.substring(7);
                login = JWTParser.parse(jwtStr).getJWTClaimsSet().getStringClaim("user_name");
                MDC.put(LOGIN_MDC_ATTR_NAME, login);
            }
        } catch (Throwable th) {
            log.warn(th.getMessage());
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);

        if (login != null) {
            MDC.remove(LOGIN_MDC_ATTR_NAME);
        }
    }
}
