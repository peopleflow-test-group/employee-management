package com.example.peopleflowdemo.employeemanagement.util;

/**
 * just auxiliary constants
 */
public class ConstUtil {

    private ConstUtil() {
    }


    /**
     * min filter order in the whole app
     */
    public static final int APP_MIN_FILTER_ORDER = -2147483643;

}
