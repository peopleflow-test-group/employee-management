package com.example.peopleflowdemo.employeemanagement.service.impl;


import com.example.peopleflowdemo.commonlib.kafka.dto.ChangeEmployeeStateKafkaDto;
import com.example.peopleflowdemo.commonlib.kafka.dto.EmployeeKafkaDto;
import com.example.peopleflowdemo.commonlib.kafka.dto.ResultKafkaDto;
import com.example.peopleflowdemo.commonlib.mapping.IEmployeeMapper;
import com.example.peopleflowdemo.commonlib.service.IReadEmployeeService;
import com.example.peopleflowdemo.commonlib.service.helper.EmployeeMappingHelper;
import com.example.peopleflowdemo.commonlib.statemachine.event.StateEvent;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeePost;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeeResponse;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeeStatePatch;
import com.example.peopleflowdemo.employeemanagement.exception.ExceptionGenerator;
import com.example.peopleflowdemo.employeemanagement.service.IAsyncEmployeeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

import static com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum.ADDED;
import static org.springframework.http.HttpStatus.*;


@RequiredArgsConstructor
@Slf4j
@Service
public class AsyncEmployeeService implements IAsyncEmployeeService {


    @Value("${kafka-topics.employee-add.name}")
    private String addTopic;

    @Value("${kafka-topics.employee-state.name}")
    private String stateTopic;

    private final ReplyingKafkaTemplate<String, EmployeeKafkaDto, ResultKafkaDto> replyingKafkaTemplate;
    private final KafkaTemplate<String, ChangeEmployeeStateKafkaDto> updateTemplate;
    private final IReadEmployeeService readEmployeeService;
    private final ExceptionGenerator exceptionGenerator;
    private final PasswordEncoder pssEncoder;
    private final IEmployeeMapper mapper;


    @Override
    public void update(Long employeeId, EmployeeStatePatch body) {

        readEmployeeService.getByIdOrElseThrow(
                employeeId,
                () -> exceptionGenerator.createLocalized(NOT_FOUND, "EMPMAN_1", null));

        ChangeEmployeeStateKafkaDto kafkaDto = new ChangeEmployeeStateKafkaDto(employeeId, StateEvent.valueOf(body.getEvent()));

        log.info("===== PRODUCER SEND =====\ntopic: {}\nbody: {}", stateTopic, kafkaDto);
        updateTemplate.send(stateTopic, kafkaDto);
    }


    @Override
    public EmployeeResponse create(EmployeePost body) {

        readEmployeeService.getByMobilePhoneThenThrow(
                body.getMobilePhone(),
                exceptionGenerator.createLocalized(BAD_REQUEST, "EMPMAN_4", null));

        EmployeeKafkaDto kafkaDto = mapper.toKafkaDto(
                body,
                EmployeeMappingHelper.createKafkaHelper(pssEncoder.encode(body.getPassword()), ADDED));

        log.info("===== PRODUCER SEND =====\ntopic: {}\nbody: {}", addTopic, kafkaDto);
        RequestReplyFuture<String, EmployeeKafkaDto, ResultKafkaDto> future = replyingKafkaTemplate
                .sendAndReceive(new ProducerRecord<>(addTopic, kafkaDto));

        ResultKafkaDto resultKafkaDto;
        try {
            resultKafkaDto = future.get().value();
        } catch (InterruptedException | ExecutionException e) {
            log.error(e.toString(), e);
            throw exceptionGenerator.createLocalized(INTERNAL_SERVER_ERROR, "EMPMAN_6", null);
        }

        log.info("===== PRODUCER RECEIVED =====\nbody: {}", kafkaDto);
        if (resultKafkaDto.getId() == null)
            throw exceptionGenerator.createLocalized(BAD_REQUEST, resultKafkaDto.getResponseCode(), null);

        return mapper.toResponse(kafkaDto, resultKafkaDto);
    }
}
