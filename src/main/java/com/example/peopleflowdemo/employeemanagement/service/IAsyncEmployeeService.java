package com.example.peopleflowdemo.employeemanagement.service;

import com.example.peopleflowdemo.commonlib.web.dto.EmployeePost;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeeResponse;
import com.example.peopleflowdemo.commonlib.web.dto.EmployeeStatePatch;

/**
 * provides async operations with employee
 */
public interface IAsyncEmployeeService {


    /**
     * init status change operation
     *
     * @param employeeId target employee id
     * @param body       info for update
     */
    void update(Long employeeId, EmployeeStatePatch body);


    /**
     * employee adding initialization process via kafka
     *
     * @param body rest request body
     * @return response body dto
     */
    EmployeeResponse create(EmployeePost body);


}
