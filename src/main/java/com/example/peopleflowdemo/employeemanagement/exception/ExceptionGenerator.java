package com.example.peopleflowdemo.employeemanagement.exception;


import com.example.peopleflowdemo.commonlib.exception.BaseRestResponseException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

/**
 * compact exception creation
 */
@RequiredArgsConstructor
@Component
public class ExceptionGenerator {


    private final MessageSource messageSource;


    public BaseRestResponseException createLocalized(HttpStatus httpStatus, String messageCode, Object[] messageParams) {
        return new BaseRestResponseException(
                httpStatus.value(),
                messageCode,
                httpStatus.getReasonPhrase(),
                httpStatus.getReasonPhrase(),
                messageSource.getMessage(messageCode, messageParams, getLocale()));
    }

}
