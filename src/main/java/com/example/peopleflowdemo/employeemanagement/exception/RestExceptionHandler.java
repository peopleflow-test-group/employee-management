package com.example.peopleflowdemo.employeemanagement.exception;

import com.example.peopleflowdemo.commonlib.exception.BaseRestResponseException;
import com.example.peopleflowdemo.commonlib.web.dto.common.CommonResponse;
import com.example.peopleflowdemo.commonlib.web.dto.common.ConstraintResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.springframework.http.ResponseEntity.unprocessableEntity;


/**
 * global exception handler for known kinds of exceptions
 */
@RequiredArgsConstructor
@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {


    /**
     * handles all validation constraints
     *
     * @param ex handled type of exception
     * @return body with detailed info per validated fields
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleArgumentNotValidException(MethodArgumentNotValidException ex) {
        log.warn(ex.toString());

        BindingResult bindingResult = ex.getBindingResult();

        if (bindingResult.hasGlobalErrors()) {
            return unprocessableEntity().body(
                    new CommonResponse(
                            applicationCode + "_422",
                            "Class level validation: " + bindingResult.getTarget().getClass().getName(),
                            bindingResult.getGlobalErrors().get(0).getDefaultMessage()));
        }

        Map<String, String> errorsMap = new HashMap<>();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        fieldErrors.forEach(fieldError -> errorsMap.put(fieldError.getField(), fieldError.getDefaultMessage()));

        return unprocessableEntity().body(new ConstraintResponse(errorsMap));
    }


    /**
     * handles rest params validation exceptions
     *
     * @param e handled type of exception
     * @return body with detailed info on exceptional case
     */
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    CommonResponse onConstraintValidationException(ConstraintViolationException e) {
        return new CommonResponse(null, e.toString(), e.getConstraintViolations().iterator().next().getMessage());
    }


    /**
     * handles app logic custom runtime exceptions
     *
     * @param e handled type of exception
     * @return body with detailed info on exceptional case
     */
    @ExceptionHandler(value = {BaseRestResponseException.class})
    @ResponseBody
    public ResponseEntity<CommonResponse> handleBaseRestResponse(BaseRestResponseException e) {
        log.warn(e.getMessage());
        return ResponseEntity.status(e.getHttpStatus()).body(e.getResponseBody());
    }


    /**
     * provides common response body for 4xx/5xx statuses
     *
     * @param e in most cases caused by headers inconsistency
     * @return common 4xx/5xx response body
     */
    @ExceptionHandler(value = {HttpMediaTypeNotSupportedException.class})
    @ResponseBody
    public ResponseEntity<CommonResponse> handleBaseRestResponse(HttpMediaTypeNotSupportedException e) {
        log.warn(e.getMessage());
        return ResponseEntity
                .status(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
                .body(new CommonResponse(null, e.toString(), null));
    }


    /**
     * internal errors lead to application custom response body with detailed info for client app
     *
     * @param e      all unforeseen exceptions
     * @param locale message locale depends on client app accept-language header
     * @return custom app response body
     */
    @ExceptionHandler(value = {Throwable.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public CommonResponse handleThrowable(Throwable e, Locale locale) {
        log.error(e.getMessage(), e);
        return new CommonResponse(
                applicationCode + "_500",
                e.toString(),
                messageSource.getMessage(applicationCode + "_500", null, locale));
    }


    private final MessageSource messageSource;

    @Value("${application.code:SYS}")
    private String applicationCode;


}

