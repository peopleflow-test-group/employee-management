# employee-management

Spring Boot service for employee management. Front controller functionality. Inits async create/update and sync read operations.

## Build

```
mvn clean package  
docker build -t mishgun8ku/employee-management:latest .  
docker push mishgun8ku/employee-management:latest  
docker run -d --rm -p8080:8080 mishgun8ku/employee-management:latest
```