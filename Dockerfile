FROM openjdk:16-jdk-alpine
MAINTAINER mikhail
COPY scripts/wait wait
RUN chmod +x wait
RUN apk --no-cache add curl
COPY target/*.jar employee-management.jar
#ENTRYPOINT ["./wait", "java", "-jar","-Dspring.profiles.active=dev", "/employee-management.jar"]
CMD ./wait && java -jar -Dspring.profiles.active=dev employee-management.jar